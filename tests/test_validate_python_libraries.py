from collections import defaultdict
from unittest import TestCase

import pytest

import validate_python_libraries


@pytest.mark.parametrize(
    ('filtered_file', 'error_dict', 'expected_error_dict'),
    (
        ("test_requirements.txt", {"test_requirements.txt": []},
         {"test_requirements.txt": [("requests", 3)]}),
        ("test_requirements_2.txt", {"test_requirements_2.txt": []},
         {"test_requirements_2.txt": [("pre-commit", 2), ("requests", 3)]}),
    )
)
def test_validate_requirements_file(filtered_file: str,
                                    error_dict: defaultdict(list),
                                    expected_error_dict: defaultdict(list)) -> None:
    validate_python_libraries.validate_requirements_file(filtered_file, error_dict)
    TestCase().assertDictEqual(expected_error_dict, error_dict)


@pytest.mark.parametrize(
    ('filtered_file', 'error_dict', 'expected_error_dict'),
    (
        ("test_glue.tfvars", {"test_glue.tfvars": []},
         {"test_glue.tfvars": [("smart-open", 1), ("requests", 1), ("pre-commit", 1)]}),
        ("test_glue_2.tfvars", {"test_glue_2.tfvars": []}, {
            "test_glue_2.tfvars": [("request", 2), ("jdcal", 2)]}),
    )
)
def test_validate_glue_file(filtered_file: str,
                            error_dict: defaultdict(list),
                            expected_error_dict: defaultdict(list)) -> None:
    validate_python_libraries.validate_glue_file(filtered_file, error_dict)
    TestCase().assertDictEqual(expected_error_dict, error_dict)


@pytest.mark.parametrize(
    ('filtered_line', 'line_number', 'filtered_file', 'error_dict', 'expected_error_dict'),
    (
        ("requests==1.2, pre-commit, jdcal", 10, "glue.tfvars", {"glue.tfvars": [("numpy", 2)]},
         {"glue.tfvars": [("numpy", 2), ("pre-commit", 10), ("jdcal", 10)]}),
        ("jdcal,   requests==1-1,     pre-commit ", 20, "glue.tfvars", {"glue.tfvars": [("numpy", 1)]},
         {"glue.tfvars": [("numpy", 1), ("jdcal", 20), ("pre-commit", 20)]}),
    )
)
def test_validate_single_library(filtered_line: str,
                                 line_number: int,
                                 filtered_file: str,
                                 error_dict: defaultdict(list),
                                 expected_error_dict: defaultdict(list)) -> None:
    validate_python_libraries.validate_single_library(filtered_line, line_number,
                                                      filtered_file, error_dict)
    TestCase().assertDictEqual(expected_error_dict, error_dict)


def check_string_exists(file_name: str, txt: str):
    with open(file_name) as dataf:
        return any(txt in line for line in dataf)


@pytest.mark.parametrize(
    ('result_file_name', 'error_dict'),
    (
        ("test_py_lib_error.log", {"glue.tfvars": [("numpy", 1)]}),
        ("test_py_lib_error.log", {"test_glue_2.tfvars": [("numpy", 2)]}),
    )
)
def test_write_validation_result(result_file_name: str,
                                 error_dict: defaultdict(list)) -> None:
    validate_python_libraries.write_validation_result(result_file_name, error_dict)

    for key, value in error_dict.items():
        if not check_string_exists(result_file_name, key):
            raise exec("%s not found in the file %s"(error_dict[0], result_file_name))
