output="$(awk 'BEGIN{RS=",";FS="\"severity\":"}  $2 ~ /Critical|High/' gl-sast-report.json)"

if [[ -n $output ]]
then
    echo "Critical or High issues are present - $output"
    exit 1
else
    echo "No Critical or High issues"
fi
