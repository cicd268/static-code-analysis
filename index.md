## Python

#### Naming conventions

- Follow [PEP-8](https://www.python.org/dev/peps/pep-0008/) standards that define coding conventions for the Python code. It is a popular guideline among Python programmers, which tells us how to name our variables, classes, and functions properly.
- Follow [PEP-257](https://www.python.org/dev/peps/pep-0257/) Docstring conventions as part of documenting your code.

#### Best Practices

- Well Structured Code- Include a README file for describing the project, the setup.py file for properly setting up your project in a new environment, and the requirements.txt for describing the dependencies that are needed. And, also documentation describing how all the components of the projects work.
- Proper documentation and comments - Two types of comments are available. One is single-line comments which start with a hash symbol (#). These types of comments can help us describe a specific line of code like what an expression does.

  ```python:
  print(“hello world”) #one line comment: this prints hello world
  ```

  The other type is a multiline comment which starts and ends with triple quotes (‘’’). They are more useful while describing a module or a function. Having meaningful comments and proper documentation improves the readability and usability of your code a lot.

- Writing Modular code - Python has many modules and libraries under a repository known as PyPI or the python package index. It contains numerous models written by Python developers, which you can implement in your code just by installing them from the internet. This prevents you from implementing all the logic from scratch and makes your script compact and readable.
- Using Virtual Environment - The main purpose of a virtual environment in Python is to create a separate environment for Python projects. This becomes particularly useful when you are using any third-party module or library in your project.

## AWS Services

Amazon already provides a huge set of developer guidelines, which should be considered when additional information is needed:

- [AWS S3](https://docs.aws.amazon.com/AmazonS3/latest/userguide/s3-userguide.pdf)
- [AWS Lambda](https://docs.aws.amazon.com/lambda/latest/dg/lambda-dg.pdf)
- [AWS Glue](https://docs.aws.amazon.com/glue/latest/dg/glue-dg.pd)
- [AWS DynamoDB](https://docs.aws.amazon.com/de_de/amazondynamodb/latest/developerguide/dynamodb-dg.pdf#Introduction)

### Amazon S3

#### Naming conventions

- Bucket names can consist only of lowercase letters, numbers, and hyphens (-). Avoid using dots (.) in bucket names for best compatibility.
- Bucket name should follow the pattern:
  ```
  bay-<program code>-<project code>-<environment>-<layer>-<region>.
  For example: bay-cph-cdp-prod-enriched-eu-central-1
  ```
- Layer part of the S3 bucket name should indicate the name of the layer the bucket is used for. Possible values can be found in the [Glossary](../glossary/index.md)
- Bucket name must be between 3 and 63 characters long and begin and end with a letter or number.
- Bucket names should be DNS-compliant.

#### Best Practices

- Implement least privilege access
- Ensure that your Amazon S3 buckets use the correct policies and are not publicly accessible
- Keep sensitive data in its own bucket with stricter security policies.
- Use IAM roles for applications and AWS services that require Amazon S3 access
- Consider VPC endpoints for Amazon S3 access. Use Amazon S3 bucket policies to control access to buckets from specific VPC endpoints, or specific VPCs.
- Enable multi-factor authentication (MFA) Delete
- Consider encryption of data at rest and in transit.
- Consider S3 Object Lock where applicable, to help prevent accidental or inappropriate deletion of data.
- Enable versioning where applicable to easily recover from both unintended user actions and application failures.
- Consider Amazon S3 cross-region replication if there are any compliance requirements.
- Enable Amazon S3 server access logging
- Proper tagging strategy should be implemented for S3 buckets for cost allocation, access control, security risk management etc.
- Implement Lifecycle policies as per archival and retention requirements.

### AWS Lambda

#### Naming conventions

- The name should follow the idioms of the language used for the lambda function.
- The name has a 64-character limit.
- Lambda function name should follow the pattern:

  ```
  bay-<program code>-<project code>-<description>
  or
  bay-<project code>-<description>
  ```

  For example: **bay-cph-cdp-enrich-s3-event-notify-imcm**, **bay-cdp-common-lambda-trggr-stpfnctnmodel**

- The name should consist only of lowercase letters, numbers, and hyphens (-).
- The name should reveal the intent of the function.

#### Best Practices

- Separate the Lambda handler from your core logic.
- Take advantage of execution environment reuse to improve the performance of your function.
- Use a keep-alive directive to maintain persistent connections.
- Use environment variables to pass operational parameters to your function.
- Control the dependencies in your function's deployment package.
- Minimize your deployment package size to its runtime necessities.
- Avoid using recursive code in your Lambda function
- Use most-restrictive permissions when setting IAM policies.
- Delete Lambda functions that you are no longer using
- Performance and load test the Lambda function to determine an optimum memory size configuration and timeout value.
- If you are using Amazon Simple Queue Service as an event source, make sure the value of the function's expected invocation time does not exceed the Visibility Timeout value on the queue.

### AWS Glue

#### Naming conventions

- The name has a 255 character limit.
- Glue ETL Jobs name should follow the pattern:
  ```
  bay-<program code>-<project code>-<description>
  or
  bay-<project code>-<description>
  ```
  For example: **bay-cph-cdp-enrich-s3-event-notify-imcm**, **bay-cdp-common-lambda-trggr-stpfnctnmodel**

#### Best Practices

- To save on cost and time while building your ETL jobs, start the development locally to test your code and business logic.
- Use a development endpoint with AWS Glue ETL to test the code on the real dataset.
- Use partitioning to filter the data at loading time instead loading the entire dataset.
- Push down predicates to prune the unnecessary partitions from the table before the underlying data is read. This is useful when there are large number of partitions in a table and only subset needs to be processed in the Glue job.
- Use Glue S3 Listener for optimized mechanism to list files on S3 while reading data into a DynamicFrame.
- Consolidate multiple files per task using the file grouping feature. Grouping files together reduces the memory footprint on the Spark driver as well as simplifying file split orchestration.
- Use S3 paths exclusions list for filtering out files that are not required by the job. This speeds job processing while reducing the memory footprint on the Spark driver.
- Use the appropriate type of scaling. Compute-intensive AWS Glue jobs that possess a high degree of data parallelism can benefit from horizontal scaling (more standard or G1.X workers). ETL jobs that need high memory or ample disk space to store intermediate shuffle output can benefit from vertical scaling (more G1.X or G2.X workers).

### Amazon DynamoDB

#### Naming conventions

- All names must be encoded using UTF-8, and are case-sensitive.
- Table names and index names must be between 3 and 255 characters long, and can contain only the following characters:

  - a-z
  - A-Z
  - 0-9
  - \_ (underscore)
  - (dash)
  - . (dot)

- Attribute names must be at least one character long, but no greater than 64 KB long. The following are the exceptions. These attribute names must be no greater than 255 characters long:
  - Secondary index partition key names.
  - Secondary index sort key names.
  - The names of any user-specified projected attributes (applicable only to local secondary indexes).

#### Best Practices

- Keep related data together. Exceptions are cases where high-volume time series data are involved, or datasets that have very different access patterns.
- Use sort order.
- Distribute queries. Design data keys to distribute traffic evenly across partitions as much as possible, avoiding "hot spots."
- Design application for uniform activity across all logical partition keys in the table and its secondary indexes.
- Design sort keys to help retrieve commonly needed groups of related items using range queries with operators such as begins_with, between, >, <, and so on.
- Use composite sort keys to define hierarchical (one-to-many) relationships in data that help query at any level of the hierarchy.
- Use global secondary indexes to enable different queries than your main table can support.
- Using Global Secondary Indexes for Materialized Aggregation Queries.
- Using Global Secondary Indexes to Create an Eventually Consistent Replica.
- Use Indexes efficiently. Keep the number of indexes to a minimum. Don't create secondary indexes on attributes that are not used often.
- Compressing large attribute values or store large attribute values in Amazon S3
- Use Adjacency List or Materialized Graph design pattern for modelling many-to-many relationships.
- Avoid scans and filters. Scan operations are less efficient than other operations in DynamoDB. A Scan operation always scans the entire table or secondary index. It then filters out values to provide the result, essentially adding the extra step of removing data from the result set.
