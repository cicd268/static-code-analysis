import os, sys, fnmatch
import re
from collections import defaultdict
from pprint import pprint

# regex pattern for the libraries with version.
regex = re.compile(r"([\w.-]+)(==)*[\w.-]+")
requirement_errors = defaultdict(list)

result_file_name = 'validate-requirements-errors.log'
error_line1 = "No library versions are mentioned in file(s)::: "
error_line2 = "##############"
error_line_end = "----------------------------------\n"

def validate_file(selected_file, requirement_errors):
    """Validate a given requirements.txt file.
       If version is not mentioned,
       update the global dictionary requirement_errors

    :param selected_file: A single file on which validation is performed.
    :param requirement_errors: Dictionary to which validation errors are added.
    """
    with open(selected_file) as current_file:
        for line in current_file:
            match_object = regex.match(line)
            if match_object is not None:
                # print the whole group
                full_library_line = match_object.group()

                # print the first group the library name
                library_name = match_object.group(1);

                # if second group does not exists, then
                # - version is not mentioned
                # - use the full_library_line
                if match_object.group(2) is None:
                    print("Library name - %s - does not have mentioned version" % (full_library_line))
                    requirement_errors[selected_file].append(full_library_line)


def write_validation_result(result_file_name):
    """Write the errors into the filename gien as argument

    :param result_file_name: File name to which errors are written
    """
    with open(result_file_name, 'w') as resultfile:
        error_line1_already_added = False
        for key, value in requirement_errors.items():
            if not error_line1_already_added:
                formatted_error_output = [error_line1, key, error_line2]
                error_line1_already_added = True;
            else:
                formatted_error_output = [key, error_line2]
            formatted_error_output.extend(value)
            formatted_error_output.append(error_line_end)
            resultfile.writelines("\n".join(formatted_error_output))
    sys.exit(1)

# Do a directory walk and find files which are python requirements.txt files.
for root, subdir, files in os.walk("."):
    for file in fnmatch.filter(files, "requirements*.txt"):
        selected_file = os.path.join(root, file)
        validate_file(selected_file, requirement_errors)

# print all the errors in the requirement_errors dict
if bool(requirement_errors):
    pprint(requirement_errors)
    write_validation_result(result_file_name)







