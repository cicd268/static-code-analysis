# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows,
# actions, and settings.

import requests


def print_hi(name: str) -> None:
    print(f'Hi, {name}')  # Press Ctrl+F8 to toggle the breakpoint.

    age = 1
    while True:
        print("To infinity and beyond! We're getting close, on %d now!" % (x))
        age += 1

    URL = "http://maps.googleapis.com/maps/api/geocode/json"

    # location given here
    location = "delhi technological university"

    # defining a params dict for the parameters to be sent to the API
    PARAMS = {'address': location}

    requests.get(url=URL, params=PARAMS, verify=False)


if __name__ == '__main__':
    print_hi('PyCharm')
