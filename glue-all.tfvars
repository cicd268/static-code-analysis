### Glue jobs
glue_jobs = {
  # Landing layer
  landing = {
    # Glue job 1
    glue_job_1 = {
      # Global
      job_country = "GLOBAL_GMDA"

      # Spark job
      glue_spark_job = "gmda-db-landing-glue-job"
      spark_s3_bucket_script_location = "releases/glue-sample/1.0.0-RELEASE/src/mygluejob.py"
      spark_glue_job_extras = {}

      # Python shell
      glue_pythonshell_job = "sfmcApiCall"
      pythonshell_s3_bucket_script_location = "releases/glue-sample/1.0.0-RELEASE/src/mygluejob.py"
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = "sfmc-classifier"
      row_tag = "Property"

      # Crawler
      glue_crawler_name = "sfmc-crawler"
      database_name = "sfmc"
      glue_crawler_s3_target = [
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Bounce",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Campaign",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Click",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Consent_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Contacts_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Jobs_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Open",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Sent",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Unsubscribe"
      ]
    }

    # Glue job 2
    glue_job_2 = {
      # Global
      job_country = "GLOBAL_GMDA"

      # Spark job
      glue_spark_job = "spark-glue-job-2"
      spark_s3_bucket_script_location = "releases/glue-sample/1.0.0-RELEASE/src/mygluejob.py"
      spark_glue_job_extras = {}

      # Python shell
      glue_pythonshell_job = "pythonshell-glue-job-2"
      pythonshell_s3_bucket_script_location = "releases/glue-sample/1.0.0-RELEASE/src/mygluejob.py"
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = "crawler-glue-job-2"
      database_name = "sfmc"
      glue_crawler_s3_target = [
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Bounce",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Campaign",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Click",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Consent_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Contacts_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Jobs_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Open",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Sent",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Unsubscribe"
      ]
    }

    #Selfservice landing Job
    bay_cdp_selfservice_landing_job = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-selfservice-landing-job"

      # Spark job
      glue_spark_job = "selfservice-landing-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-selfservice-landing-job-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-selfservice-landing-job-eu-central-1.py"
      spark_glue_job_extras = {
      }
      spark_glue_job_additional_arguments = {
        "--additional-python-modules" = "openpyxl==1.3,xlrd==2.0.1, jdcal, et_xmlfile"
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "30"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
   #Selfservice raw Job
    bay_cdp_selfservice_raw_job = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-selfservice-raw-job"

      # Spark job
      glue_spark_job = "selfservice-raw-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-selfservice-raw-job-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-selfservice-raw-job-eu-central-1.py"
      spark_glue_job_extras = {
      }
      spark_glue_job_additional_arguments = {
        "--additional-python-modules" = "openpyxl==1.2,xlrd==2.0.1,jdcal==1.4.1,et_xmlfile==1.0.1"
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "30"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
    #Selfservice enrich Job
    bay_cdp_selfservice_enrich_job = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-selfservice-enrich-job"

      # Spark job
      glue_spark_job = "selfservice-enrich-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-selfservice-enrich-job-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-selfservice-enrich-job-eu-central-1.py"
      spark_glue_job_extras = {
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "30"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
        #scienceatscale landing Job
    bay_cdp_scienceatscale_landing_job = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-scienceatscale-landing-job"

      # Spark job
      glue_spark_job = "scienceatscale-landing-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-scienceatscale-landing-job-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-scienceatscale-landing-job-eu-central-1.py"
      spark_glue_job_extras = {
      }
      spark_glue_job_additional_arguments = {
        "--additional-python-modules" = "openpyxl==3.0.6,xlrd==2.0.1,jdcal==1.4.1,et_xmlfile==1.0.1"
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "30"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
   #Selfservice raw Job
    bay_cdp_scienceatscale_raw_job = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-scienceatscale-raw-job"

      # Spark job
      glue_spark_job = "scienceatscale-raw-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-scienceatscale-raw-job-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-scienceatscale-raw-job-eu-central-1.py"
      spark_glue_job_extras = {
      }
      spark_glue_job_additional_arguments = {
        "--additional-python-modules" = "openpyxl==3.0.6,xlrd==2.0.1,jdcal==1.4.1,et_xmlfile==1.0.1"
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "30"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
        #Scienceatscale File validation Job
    bay_cdp_scienceatscale_validation = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-scienceatscale-validation"

      # Spark job
      glue_spark_job = "scienceatscale-validation"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-scienceatscale-validation-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-scienceatscale-validation-eu-central-1.py"
      spark_glue_job_extras = {}
      spark_glue_job_additional_arguments= {
        "--JOB_NAME" = "",
        "--EMAIL_ID" = "",
        "--S3_FILE_KEY" = "",
        "--MYBUCKET" = ""
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "30"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

    #Selfservice enrich Job
    bay_cdp_scienceatscale_enrich_job = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-scienceatscale-enrich-job"

      # Spark job
      glue_spark_job = "scienceatscale-enrich-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-scienceatscale-enrich-job-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-scienceatscale-enrich-job-eu-central-1.py"
      spark_glue_job_extras = {
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "30"

      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }


    #bay-cdp-collibra-glue-catalog
    bay-cdp-collibra-glue-catalog = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-collibra-glue-catalog"

      # Spark job
      glue_spark_job = ""
      spark_s3_bucket_script_location = ""
      spark_glue_job_extras = {
      }
      spark_glue_job_additional_arguments = {
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "6"

      # Python shell
      glue_pythonshell_job = "collibra-glue-catalog"
      pythonshell_s3_bucket_script_location = "snapshots/bay-cdp-collibra-glue-catalog/1.0.0-SNAPSHOT/src/bay-cdp-collibra-glue-catelog-eu-central-1.py"
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = [
      ]
    }
     # Medical Selfservice landing Job
    bay_cdp_medical_landing_job = {
      # Global
      job_region = "GLOBAL"
      job_name = "bay-cdp-medical-landing-job"

      # Spark job
      glue_spark_job = "medical-landing-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-medical-landing-job-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-medical-landing-job-eu-central-1.py"
      spark_glue_job_extras = {
      }
      spark_glue_job_additional_arguments = {
        "--additional-python-modules" = "openpyxl==3.0.6,xlrd==2.0.1,jdcal==1.4.1,et_xmlfile==1.0.1"
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "30"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
    bay-cdp-snwflk-pyshell-enrich-retrigger-job= {
      ## Global
      job_country = "GLOBAL"
      job_region = ""
      job_name = "bay-cdp-snwflk-pyshell-enrich-retrigger-job"

      # Spark job
      glue_spark_job = ""
      spark_s3_bucket_script_location = ""
      spark_glue_job_extras = {}

      # Python shell
      glue_pythonshell_job = "snwflk-pyshell-enrich-retrigger-job"
      pythonshell_python_version = "3.9"
      pythonshell_glue_version = "3.0"
      pythonshell_s3_bucket_script_location = "snapshots/bay-cdp-snwflk-pyshell-enrich-retrigger-job/1.0.0-SNAPSHOT/src/bay-cdp-snwflk-pyshell-enrich-retrigger-job.py"
      pythonshell_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/python-packages/snapshots/cdp_commons/1.0.0-SNAPSHOT/cdp_commons-1.0.0.dev0-py3-none-any.whl,s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/glue/snapshots/bay-cdp-snwflk-pyshell-enrich-retrigger-job/1.0.0-SNAPSHOT/extra-py-files/snowflake_connector_python-2.7.12-cp39-cp39-manylinux_2_17_x86_64.manylinux2014_x86_64.whl",
        "--JOB_RUN_COUNT" = "0",
        "--ENTITY_NAME" = ""

       }
       pythonshell_max_concurrent_runs	= "20"
       pythonshell_number_of_workers =  "20"

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

    bay_cdp_anlyt_hist_pythonshell_job = {
      # Global
      job_country = "GLOBAL"
      job_region = "EU"
      job_name = "bay-cdp-anlyt-hist-pythonshell-job"

      # Spark job
      #glue_spark_job = "model-spark-job"
      glue_spark_job = ""
      spark_s3_bucket_script_location = ""
      spark_glue_job_extras = {}

	    glue_pythonshell_job = "anlyt-hist-pythonshell-job"
      pythonshell_s3_bucket_script_location = "snapshots/bay-cdp-anlyt-hist-pythonshell-job-eu-central-1/1.0.0-SNAPSHOT/src/bay-cdp-anlyt-hist-pythonshell-job-eu-central-1.py"
      pythonshell_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/glue/snapshots/bay-cdp-anlyt-hist-pythonshell-job-eu-central-1/1.0.0-SNAPSHOT/extra-py-files/snowflake_connector_python-2.3.6-cp36-cp36m-manylinux1_x86_64.whl",
        "--LOAD_TYPE" = "I",
        "--PROCESS_ID" = "ANALYTICAL_EU"
       }

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

#adhoccustom us-east-1 landing job
bay_cdp_adhoccustom_landing_job_us_east_1 = {
      ## Global
      job_region = "US"
      job_country = "US"
      job_name = "bay-cdp-adhoccustom-landing-job-us-east-1"
      source_name = "ADHOCCUSTOM"

      # Spark job
      glue_spark_job = "adhoccustom-landing-job"
      spark_s3_bucket_script_location = "snapshots/bay_cdp_adhoccustom_landing_job_us_east_1/1.0.0-SNAPSHOT/src/bay_cdp_adhoccustom_landing_job_us_east_1.py"
      spark_glue_job_extras = {}

      spark_glue_job_additional_arguments = {
       "--additional-python-modules" = "cryptography==3.3.2,paramiko==2.7.2,xlrd==1.1.0"
       }

      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "10"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}
      pythonshell_glue_job_additional_arguments = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

#adhoccustom Enrich job us-east-1
    bay_cdp_adhoccustom_enrich_job_us_east_1 = {
      # Global
      job_country = "US"
      source_name = "ADHOCCUSTOM"
      job_region = "US"
      job_name = "bay-cdp-adhoccustom-enrich-job-us-east-1"

      # Spark job
      glue_spark_job = "adhoccustom-enrich-job"
      spark_s3_bucket_script_location = "snapshots/bay_cdp_adhoccustom_enrich_job_us_east_1/1.0.0-SNAPSHOT/src/bay_cdp_adhoccustom_enrich_job_us_east_1.py"
      spark_glue_job_extras = {
        "--extra-py-files" = ""
      }
      spark_glue_job_additional_arguments = {
        "--additional-python-modules" = "daylight_python_packages==1.0.0.dev0"
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "10"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      spark_glue_job_extras = {}

      # Python shell
      glue_pythonshell_job = "evolution-migration-model-outer-pythonshell-job"
      pythonshell_s3_bucket_script_location = "releases/evolution-migration-model-outer-pythonshell-job-us-east-1/1.0.4-RELEASE/src/evolution-migration-model-outer-pythonshell-job-us-east-1.py"
      pythonshell_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-us-east-1/glue/releases/evolution-migration-model-outer-pythonshell-job-us-east-1/1.0.4-RELEASE/extra-py-files/snowflake_connector_python-2.3.6-cp36-cp36m-manylinux1_x86_64.whl"
       }
       pythonshell_glue_job_additional_arguments = {
        "--BUCKET_NAME" = "by-cdp-code-cofig-pre-dev",
        "--FILE_PATH" = "gluecode/reff-file/LOAD_ID.txt",
        "--ENV"  = "dev",
		"--EVOL_WAREHOUSE" = "1",
		"--TENENT_NM" = "us-east-1",
		"--DATABASE" = "1"
      }
      pythonshell_max_concurrent_runs	= "20"

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
    # veevanetwork Enrich us-east-1
    bay-cdp-veevanetwork-enrich-job-us-east-1-refactored = {
      # Global
      job_country = "US"
      source_name = "VEEVANETWORK"
      job_region = "US"
      job_name = "bay-cdp-veevanetwork-enrich-refactored-job-us-east-1"

      # Spark job
      glue_spark_job = "veevanetwork-enrich-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-veevanetwork-enrich-job-us-east-1-refactored/1.0.0-SNAPSHOT/src/bay-cdp-veevanetwork-enrich-job-us-east-1-refactored.py"
      spark_glue_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/glue/snapshots/bay-cdp-veevanetwork-enrich-job-us-east-1-refactored/1.0.0-SNAPSHOT/extra-py-files/UDFPackage.zip"
      }
      spark_glue_job_additional_arguments = {
      "--additional-python-modules" = "cdp_commons==1.0.9"
      }

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

    # veevanetwork Enrich us-east-1
    by_cdp_veevanetwork_enrich_job_us_east_1 = {
      # Global
      job_country = "US"
      source_name = "VEEVANETWORK"
      job_region = "US"
      job_name = "bay-cdp-veevanetwork-enrich-job-us-east-1"

      # Spark job
      glue_spark_job = "veevanetwork-enrich-job"

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

    bay-cdp-anlyt-evol-pythonshell-job-us-east-1-refactored = {
      # Global
      job_country = "GLOBAL"
      job_region = "US"
      job_name = "bay-cdp-anlyt-evol-pythonshell-refactored-job-us-east-1"

      # Spark job
      glue_spark_job = ""
      spark_s3_bucket_script_location = ""
      spark_glue_job_extras = {}

      # Python shell
      glue_pythonshell_job = "anlyt-evol-pythonshell-refactored-job"
      pythonshell_s3_bucket_script_location = "snapshots/bay-cdp-anlyt-evol-pythonshell-job-us-east-1-refactored/1.0.0-SNAPSHOT/src/bay-cdp-anlyt-evol-pythonshell-job-us-east-1-refactored.py"
      pythonshell_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/glue/snapshots/bay-cdp-anlyt-evol-pythonshell-job-us-east-1-refactored/1.0.0-SNAPSHOT/extra-py-files/snowflake_connector_python-2.3.6-cp36-cp36m-manylinux1_x86_64.whl",
        "--LOAD_TYPE" = "I",
        "--PROCESS_ID" = "ANALYTICAL_R_EVOL_US"
       }
       pythonshell_max_concurrent_runs	= "20"

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }


  }
}

glue_jobs_ap_southeast_1 = {
  # Landing layer
  landing = {
    # Glue job 1
    glue_job_1 = {
      # Global
      job_country = "GLOBAL_GMDA"

      # Spark job
      glue_spark_job = "regional-spark-job"
      spark_s3_bucket_script_location = "releases/glue-sample/1.0.0-RELEASE/src/mygluejob.py"
      spark_glue_job_extras = {}

      # Python shell
      glue_pythonshell_job = "regional-pyshell-job"
      pythonshell_s3_bucket_script_location = "releases/glue-sample/1.0.0-RELEASE/src/mygluejob.py"
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = "regional-classifier"
      row_tag = "Property"

      # Crawler
      glue_crawler_name = "regional-crawler"
      database_name = "sfmc"
      glue_crawler_s3_target = [
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Bounce",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Campaign",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Click",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Consent_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Contacts_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Jobs_Master",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Open",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Sent",
        "s3://by-cdp-landing-dev/SFMC/20200910171734/IMCM_CPHEU_Unsubscribe"
      ]
    }
    bay_cdp_common_reprocessing_job_ap_southeast_1 = {
      # Global
      job_country = "AP"
      job_region = "AP"
      job_name = "bay-cdp-common-reprocessing-job-ap-southeast-1"
      source_name = "SFMC"

      # Spark job
      glue_spark_job = ""
      spark_s3_bucket_script_location = ""
      spark_glue_job_extras = {}

      # Python shell
	    glue_pythonshell_job = "common-reprocessing-job"
      pythonshell_s3_bucket_script_location = "snapshots/bay-cdp-common-reprocessing-job-ap-southeast-1/1.0.0-SNAPSHOT/src/bay-cdp-common-reprocessing-job-ap-southeast-1.py"
      pythonshell_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/glue/snapshots/bay-cdp-common-reprocessing-job-ap-southeast-1/1.0.0-SNAPSHOT/extra-py-files/snowflake_connector_python-2.3.6-cp36-cp36m-manylinux1_x86_64.whl",
        "--LOAD_TYPE" = "I"
       }
       pythonshell_max_concurrent_runs	= "20"

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
     #briefing document Raw layer job
   bay_cdp_briefing_document_raw_job_ap_southeast_1= {
      # Global
      job_country = "JP"
      source_name = "BRIEFINGDOCUMENT"
      job_region = "AP"
      job_name = "bay-cdp-briefing-document-raw-job-ap-southeast-1"

      # Spark job
      glue_spark_job = "briefing-document-raw-job"
      spark_s3_bucket_script_location = "releases/bay-cdp-briefing-document-raw-job-ap-southeast-1/1.0.2-RELEASE/src/bay-cdp-briefing-document-raw-job-ap-southeast-1.py"
      spark_glue_job_extras = {}
      spark_glue_job_additional_arguments = {
        "--additional-python-modules" = "cdp_commons==1.2.5, xlrd==1.1.0"
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "20"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
      database_name = ""
      glue_crawler_s3_target = []
    }
    #Refactored model job
    bay_cdp_model_pythonshell_job_ap_southeast_1_refactored= {
      # Global
      job_country = "GLOBAL"
      job_region = "AP"
      job_name = "bay-cdp-model-pythonshell-refactored-job-ap-southeast-1"

      # Spark job
      glue_spark_job = ""
      spark_s3_bucket_script_location = ""
      spark_glue_job_extras = {}

      # Python shell
      glue_pythonshell_job = "model-pythonshell-refactored-job"
      #max_concurrent_runs = "20"
      pythonshell_s3_bucket_script_location = "snapshots/bay-cdp-model-pythonshell-job-ap-southeast-1-refactored/1.0.0-SNAPSHOT/src/bay-cdp-model-pythonshell-job-ap-southeast-1-refactored.py"
      pythonshell_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-model-pythonshell-job-ap-southeast-1-refactored/1.0.0-SNAPSHOT/extra-py-files/snowflake_connector_python-2.3.6-cp36-cp36m-manylinux1_x86_64.whl,s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/python-packages/snapshots/cdp_commons/1.0.0-SNAPSHOT/cdp_commons-1.0.0.dev0-py3-none-any.whl"
       }

      pythonshell_max_concurrent_runs	= "20"
      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

     bay_cdp_common_model_job_ap_southeast_1 = {
      # Global
      job_country = "GLOBAL"
      job_region = "AP"
      job_name = "bay-cdp-model-pythonshell-job-ap-southeast-1"

      # Spark job
      #glue_spark_job = "model-spark-job"
      glue_spark_job = ""
      #spark_s3_bucket_script_location = "snapshots/mygluejob1/1.0.2-SNAPSHOT/src-202009251243/myscript.py"
      spark_s3_bucket_script_location = ""
      spark_glue_job_extras = {}

      # Python shell
      pythonshell_python_version = "3.9"
      pythonshell_glue_version = "3.0"
      glue_pythonshell_job = "model-pythonshell-job"
      #max_concurrent_runs = "20"
      pythonshell_s3_bucket_script_location = "snapshots/bay-cdp-model-pythonshell-job-ap-southeast-1/1.0.0-SNAPSHOT/src/bay-cdp-model-pythonshell-job-ap-southeast-1.py"
      pythonshell_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/python-packages/release/cdp_commons/1.3.4-RELEASE/cdp_commons-1.3.4-py3-none-any.whl,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/releases/bay-cdp-model-pythonshell-job-ap-southeast-1/1.4.1-RELEASE/extra-py-files/smart_open-5.2.1-py3-none-any.whl,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/releases/bay-cdp-model-pythonshell-job-ap-southeast-1/1.4.1-RELEASE/extra-py-files/snowflake_connector_python-2.7.12-cp39-cp39-manylinux_2_17_x86_64.manylinux2014_x86_64.whl",
        "--GROUP_ID" = "0"
       }
      pythonshell_glue_job_additional_arguments = {
        "--additional-python-modules" = "smart-open==5.2.1"
      }
      pythonshell_max_concurrent_runs	= "20"
      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
      spark_glue_job_additional_arguments = {
       "--additional-python-modules" = "cdp_commons==1.2.7,UDFPackage==1.0.1"
      }

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
    # veevanetwork Enrich APAC
    bay-cdp-veevanetwork-enrich-job-ap-southeast-1-refactored = {
      # Global
      job_country = "AP"
      source_name = "VEEVANETWORK"
      job_region = "AP"
      job_name = "bay-cdp-veevanetwork-enrich-refactored-job-ap-southeast-1"

      # Spark job
      glue_spark_job = "veevanetwork-enrich-refactored-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-veevanetwork-enrich-job-ap-southeast-1-refactored/1.0.0-SNAPSHOT/src/bay-cdp-veevanetwork-enrich-job-ap-southeast-1-refactored.py"
      spark_glue_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/glue/snapshots/bay-cdp-veevanetwork-enrich-job-ap-southeast-1-refactored/1.0.0-SNAPSHOT/extra-py-files/UDFPackage.zip"
      }
      spark_glue_job_additional_arguments = {
      "--additional-python-modules" = "cdp_commons==1.0.9"
      }

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }


    #systenant report us-east-1 job
    by_cdp_systenant_report_job_ap_southeast_1 = {
      # Global
      job_country = ""
      job_region = ""
      job_name = "bay-cdp-systenant-report-ap-southeast-1"
      source_name = ""


      # Spark job
      glue_spark_job = "systenant-report"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-systenant-report-ap-southeast-1/1.0.0-SNAPSHOT/src/bay-cdp-systenant-report-ap-southeast-1.py"
      spark_glue_job_extras = {}
      spark_glue_job_additional_arguments = {
        "--additional-python-modules" = "pyarrow==2,awswrangler==0.9,fsspec==0.7.4",
        "--SCHEMA_LIST" = "enriched_veevacrm_ec1,enriched_veevacrm_ec2",
        "--ATHENA_S3_OUTPUT"  = "s3://bay-cph-athena-results-eu-central-1/results/",
		    "--S3_OUTPUT" = "s3://bay-cph-cdp-dev-enriched-eu-central-1/SYSTENANT_REPORT/"
      }
      spark_glue_job_additional_arguments = {
        "--LOAD_ID" = "20210413090000",
        "--SOURCE_NAME" = "VEEVA_CUSTOMER_MDM"

        }

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

    #MDM Maintenance ap-southeast-1 job
    bay_cdp_mdm_maintenance_job_ap_southeast_1 = {
      # Global
      job_country = "PC3"
      job_region = "AP"
      job_name = "bay-cdp-mdm-maintenance-job-ap-southeast-1"

      # Spark job
      glue_spark_job = "mdm-maintenance-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-mdm-maintenance-job-ap-southeast-1/1.0.0-SNAPSHOT/src/bay-cdp-mdm-maintenance-job-ap-southeast-1.py"
      spark_glue_job_extras = {

      "--extra-files" = "s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-maintenance-job-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/hco-config.properties,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-maintenance-job-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/hcp-config.properties,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-maintenance-job-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/rs-config.properties,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-maintenance-job-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/del-rs-config.properties,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-maintenance-job-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/mp-config.properties,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-maintenance-job-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/pg-config.properties"

      }
      spark_glue_job_additional_arguments = {
        "--LOAD_ID" = "20210413090000",
        "--SOURCE_NAME" = "MAINTENANCE_MDM"

        }

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
    #MDM Product Outbound ap-southeast-1 job
    bay_cdp_mdm_outbound_ap_southeast_1 = {
      # Global
      job_country = "GLOBAL"
      job_region = "AP"
      job_name = "bay-cdp-mdm-outbound-job-ap-southeast-1"

      # Spark job
      glue_spark_job = "mdm-outbound-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-mdm-outbound-ap-southeast-1/1.0.0-SNAPSHOT/src/bay-cdp-mdm-outbound-ap-southeast-1.py"
      spark_glue_job_extras = {
        "--extra-files" = "s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-outbound-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/attribute-mapping-1.properties,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-outbound-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/outbound-pg-config.properties,s3://bay-cph-cpd-tooling-code-distribution-ap-southeast-1/glue/snapshots/bay-cdp-mdm-outbound-ap-southeast-1/1.0.0-SNAPSHOT/extra-files/outbound-mp-config.properties"
      }
      spark_glue_job_additional_arguments = {

        "--SOURCE_NAME" = "PRODUCT_MDM_OUTBOUND"

        }

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
    #dq landing enriched job
    bay-cdp-dq-landing-enriched-recon-job-ap-southeast-1 = {
      # Global
      job_country = ""
      source_name = "DQ_RECON"
      job_region = "ap-southeast-1"
      job_name = "bay-cdp-dq-landing-enriched-recon-job-ap-southeast-1"

      # Spark job
      glue_spark_job = "dq-landing-enriched-recon-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-dq-landing-enriched-recon-job-ap-southeast-1/1.0.0-SNAPSHOT/src/bay-cdp-dq-landing-enriched-recon-job-ap-southeast-1.py"
      spark_glue_job_extras = {}
      spark_glue_job_additional_arguments	= {"--additional-python-modules" = "cdp_commons==1.2.9"}
      spark_number_of_workers =  "21"
      spark_max_concurrent_runs = "6"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
    #adhoc-history-data-load job
    bay-cdp-jpc-adhoc-history-data-load-job-ap-southeast-1 = {
      # Global
      job_country = "JP"
      source_name = "ANALYTICS_JP"
      job_region = "AP"
      job_name = "bay-cdp-jpc-adhoc-history-data-load-job-ap-southeast-1"

      # Spark job
      glue_spark_job = "jpc-adhoc-history-data-load-job"
      spark_s3_bucket_script_location = "snapshots/bay-cdp-jpc-adhoc-history-data-load/1.0.0-SNAPSHOT/src/bay-cdp-jpc-adhoc-history-data-load.py"
      spark_glue_job_extras = {}
      spark_glue_job_additional_arguments	= {
        "--additional-python-modules" = "cdp_commons==1.3.2,smart-open==5.2.1",
        "--LOAD_ID" = "0",
        "--ENTITY_NAME" = "",
        "--S3_PATH" =""
      }
      spark_number_of_workers =  "20"
      spark_max_concurrent_runs = "6"

      # Python shell
      glue_pythonshell_job = ""
      pythonshell_s3_bucket_script_location = ""
      pythonshell_job_extras = {}

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }
    #evol refactored job
    bay-cdp-anlyt-evol-pythonshell-job-ap-southeast-1-refactored = {
      # Global
      job_country = "GLOBAL"
      job_region = "AP"
      job_name = "bay-cdp-anlyt-evol-pythonshell-refactored-job-ap-southeast-1"

      # Spark job
      glue_spark_job = ""
      spark_s3_bucket_script_location = ""
      spark_glue_job_extras = {}

      # Python shell
      glue_pythonshell_job = "anlyt-evol-pythonshell-refactored-job"
      pythonshell_s3_bucket_script_location = "snapshots/bay-cdp-anlyt-evol-pythonshell-job-ap-southeast-1-refactored/1.0.0-SNAPSHOT/src/bay-cdp-anlyt-evol-pythonshell-job-ap-southeast-1-refactored.py"
      pythonshell_job_extras = {
        "--extra-py-files" = "s3://bay-cph-cpd-tooling-code-distribution-eu-central-1/glue/snapshots/bay-cdp-anlyt-evol-pythonshell-job-ap-southeast-1-refactored/1.0.0-SNAPSHOT/extra-py-files/snowflake_connector_python-2.3.6-cp36-cp36m-manylinux1_x86_64.whl",
        "--LOAD_TYPE" = "I",
        "--PROCESS_ID" = "ANALYTICAL_R_EVOL_AP"
       }
       pythonshell_max_concurrent_runs	= "20"

      # Classifier
      glue_classifier_name = ""
      row_tag = ""

      # Crawler
      glue_crawler_name = ""
      database_name = ""
      glue_crawler_s3_target = []
    }

  }
}

glue_triggers = {
  landing = {
    # glue_trigger_test_on_demand = {
    #   name = "glue_trigger_test_on_demand"
    #   glue_trigger_actions = {
    #     test = {
    #       job_name = "test_glue_job"
    #     }
    #   }
    # }

    glue_trigger_athenareport_EU = {
       name = "glue_trigger_athenareport_EU"
       type = "SCHEDULED"
       schedule = "cron(0 7 ? * WED *)"
       glue_trigger_actions = {
         test = {
           job_name = "athena-sourcemetadata-compare-report"

         }

       }
     }



    # glue_trigger_test_scheduled = {
    #   name = "glue_trigger_test_scheduled"
    #   type = "SCHEDULED"
    #   schedule = "cron(15 12 * * ? *)"
    #   glue_trigger_actions = {
    #     test = {
    #       job_name = "test_glue_job"
    #     }
    #   }
    # }
    # glue_trigger_test_conditional_glue_job = {
    #   name = "glue_trigger_test_conditional_glue_job"
    #   type = "CONDITIONAL"
    #   glue_trigger_actions = {
    #     job_name = "test_glue_job"
    #   }
    #   glue_trigger_predicate = {
    #     test = {
    #       job_name = "test2_glue_job"
    #       state = "SUCCEEDED"
    #     }
    #   }
    # }
    # glue_trigger_test_conditional_glue_crawler = {
    #   name = "glue_trigger_test_conditional_glue_crawler"
    #   type = "CONDITIONAL"
    #   glue_trigger_actions = {
    #     job_name = "test_glue_job"
    #   }
    #   glue_trigger_predicate = {
    #     test = {
    #       crawler_name  = "test_glue_crawler"
    #       crawl_state   = "SUCCEEDED"
    #     }
    #   }
    # }
    # glue_trigger_crawler_conditional_glue_job = {
    #   name = "glue_trigger_crawler_conditional_glue_job"
    #   type = "CONDITIONAL"
    #   glue_trigger_actions = {
    #     crawler_name  = "test_glue_crawler"
    #   }
    #   glue_trigger_predicate = {
    #     test = {
    #       job_name = "test_glue_job"
    #       state = "SUCCEEDED"
    #     }
    #   }
    # }
    glue_trigger_dq_enriched_model_recon = {
       name = "glue_trigger_dq_enriched_model_recon"
       type = "SCHEDULED"
       schedule = "cron(0 21 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "dq-enriched-model-recon-job"
           arguments = {
           "--SOURCE_NAME" = "DQ_RECON",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_NAME" = "bay-cdp-dq-enriched-model-recon-job",
           "--JOB_REGION" = "GLOBAL"
         }
         }

     glue_trigger_nl_connectivity = {
       name = "glue_trigger_nl_connectivity"
       type = "SCHEDULED"
       schedule = "cron(0 14 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "localdata-ftp-connection-job"
           arguments = {
           "--SOURCE_NAME" = "ANALYTICS_NL",
           "--JOB_RUN" = "1",
           "--JOB_COUNTRY" = "NL",
           "--JOB_REGION" = "EU"
         }
         }

       }
     }

     glue_trigger_RU_connectivity = {
       name = "glue_trigger_RU_connectivity"
       type = "SCHEDULED"
       schedule = "cron(0 18 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "localdata-sqlsrvr-connection-job"
           arguments = {
           "--SOURCE_NAME" = "REFERENCE_RU",
           "--JOB_RUN" = "1",
           "--JOB_COUNTRY" = "RU",
           "--LOAD_ID" = "1",
           "--JOB_REGION" = "EU"
         }
         }

       }
     }


     glue_trigger_googleanalytics_EU = {
       name = "glue_trigger_googleanalytics_EU"
       type = "SCHEDULED"
       schedule = "cron(0 22 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "googleanalytics-prelanding-job"
           arguments = {
           "--SOURCE_NAME" = "GOOGLEANALYTICS",
           "--JOB_RUN" = "1",
           "--JOB_COUNTRY" = "EU",
           "--JOB_REGION" = "EU",
           "--VIEWIDS" = "ALL",
           "--LOAD_TYPE" = "INCREMENTAL"
         }
         }

       }
     }
     glue_trigger_dq_recon_eu = {
       name = "glue_trigger_dq_recon_eu"
       type = "SCHEDULED"
       schedule = "cron(0 21 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "dq-landing-enriched-recon-job"
           arguments = {
           "--SOURCE_NAME" = "DQ_RECON",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_NAME" = "bay-cdp-dq-landing-enriched-recon-job-eu-central-1",
           "--JOB_REGION" = "eu-central-1"
         }
         }

       }
     }
    glue_trigger_dq_enriched_model_recon_eu = {
       name = "glue_trigger_dq_enriched_model_recon_eu"
       type = "SCHEDULED"
       schedule = "cron(0 21 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "dq-enriched-model-recon-job"
           arguments = {
           "--SOURCE_NAME" = "DQ_RECON",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_NAME" = "bay-cdp-dq-enriched-model-recon-job-eu-central-1",
           "--JOB_REGION" = "eu-central-1"
         }
         }

       }
     }
     glue_trigger_dq_landing_enriched_recon_eu = {
       name = "glue_trigger_dq_landing_enriched_recon_eu"
       type = "SCHEDULED"
       schedule = "cron(0 21 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "dq-landing-enriched-recon-job"
           arguments = {
           "--SOURCE_NAME" = "DQ_RECON",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_NAME" = "bay-cdp-dq-landing-enriched-recon-job-eu-central-1",
           "--JOB_REGION" = "eu-central-1"
         }
         }

       }
     }
    #   glue_trigger_conditional_bay_cdp_mdm_veeva_enrich_job_eu_central_1 = {
		# name = "bay-cdp-mdm-veeva-enrich-job-eu-central-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 3/6 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "mdm-veeva-enrich-job"
    #           arguments = {
    #           "--JOB_COUNTRY" = "EC1"
    #           }
    #       }
    #      }
    #   }
    #   glue_trigger_conditional_bay_cdp_mdm_veeva2_enrich_job_eu_central_1 = {
		# name = "bay-cdp-mdm-veeva2-enrich-job-eu-central-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 6/6 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "mdm-veeva-enrich-job"
    #           arguments = {
    #           "--JOB_COUNTRY" = "EC2"
    #           }
    #       }
    #      }
    #   }
    #   glue_trigger_conditional_bay_cdp_mdm_veeva_customer_enrich_job_eu_central_1 = {
		# name = "bay-cdp-mdm-veeva-customer-enrich-job-eu-central-1_trigger"
    #      }
    #   }
    #   glue_trigger_conditional_bay_cdp_mdm_sap_enrich_job_eu_central_1 = {
		# name = "bay-cdp-mdm-sap-enrich-job-eu-central-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 3 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "mdm-sap-enrich-job"
    #       }
    #      }
    #   }
     # bay_cdp_collibra_glue_catalog_eu_central_1_trg
     bay_cdp_collibra_glue_catalog_eu_central_1_trg = {
       name = "bay_cdp_collibra_glue_catalog_eu_central_1_trg"
       type = "SCHEDULED"
       schedule = "cron(0 13 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "collibra-glue-catalog"
           arguments = {
           "--SOURCE_NAME" = "",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_COUNTRY" = "",
           "--JOB_REGION" = "EU"
         }
         }

       }
     }
    #  glue_trigger_conditional_bay_cdp_mdm_sap_customer_enrich_job_eu_central_1 = {
		# name = "bay-cdp-mdm-sap-customer-enrich-job-eu-central-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 3 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "mdm-sap-customer-enrich-job"
    #           arguments = {
    #           "--JOB_COUNTRY" = "EU"
    #           }
    #       }
    #      }
    #   }
      glue_trigger_RU_connectivity_weeklyREF = {
       name = "glue_trigger_RU_connectivity_weeklyREF"
       type = "SCHEDULED"
       schedule = "cron(0 13 ? * SAT *)"
       glue_trigger_actions = {
         test = {
           job_name = "localdata-sqlsrvr-connection-job"
           arguments = {
           "--SOURCE_NAME" = "REFERENCE_RU",
           "--JOB_RUN" = "1",
           "--JOB_COUNTRY" = "RU",
           "--LOAD_ID" = "1",
           "--JOB_REGION" = "EU",
           "--GROUP_ID" = "weeklytriggerREF"
         }
         }

       }
     }
     glue_trigger_RU_connectivity_weeklyANL = {
       name = "glue_trigger_RU_connectivity_weeklyANL"
       type = "SCHEDULED"
       schedule = "cron(0 14 ? * SAT *)"
       glue_trigger_actions = {
         test = {
           job_name = "localdata-sqlsrvr-connection-job"
     }

     #Commented as per request
     glue_trigger_sfmc_enriched_US = {
       name = "glue_trigger_sfmc_enriched_US"
       type = "SCHEDULED"
       schedule = "cron(30 0 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "sfmc-enriched-ldrshp-dshbrd"
           arguments = {
           "--SOURCE_NAME" = "SFMC",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_COUNTRY" = "US",
           "--JOB_REGION" = "US"
         }
         }

       }
     }

     # bay_cdp_collibra_glue_catalog_us_east_1_trg
     bay_cdp_collibra_glue_catalog_us_east_1_trg = {
       name = "bay_cdp_collibra_glue_catalog_us_east_1_trg"
       type = "SCHEDULED"
       schedule = "cron(0 13 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "collibra-glue-catalog"
           arguments = {
           "--SOURCE_NAME" = "",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_COUNTRY" = "",
           "--JOB_REGION" = "US"
         }
         }

       }
     }
     glue_trigger_dq_enriched_model_recon_us = {
       name = "glue_trigger_dq_enriched_model_recon_us"
       type = "SCHEDULED"
       schedule = "cron(0 21 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "dq-enriched-model-recon-job"
           arguments = {
           "--SOURCE_NAME" = "DQ_RECON",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_NAME" = "bay-cdp-dq-enriched-model-recon-job-us-east-1",
           "--JOB_REGION" = "us-east-1"
         }
         }

       }
     }

     glue_trigger_dq_landing_enriched_recon_us = {
       name = "glue_trigger_dq_landing_enriched_recon_us"
       type = "SCHEDULED"
       schedule = "cron(0 21 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "dq-landing-enriched-recon-job"
           arguments = {
           "--SOURCE_NAME" = "DQ_RECON",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_NAME" = "bay-cdp-dq-landing-enriched-recon-job-us-east-1",
           "--JOB_REGION" = "us-east-1"
         }
         }

       }
     }
    #  glue_trigger_conditional_bay_cdp_mdm_veeva_customer_enrich_job_us_east_1_AC1 = {
		# name = "bay-cdp-mdm-veeva-customer-enrich-job-us-east-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 3/6 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "mdm-veeva-customer-enrich-job"
    #           arguments = {
    #           "--JOB_COUNTRY" = "AC1"
    #           }
    #       }
    #      }
    #   }
    #   glue_trigger_bay_cdp_cust_mdm_outbound_job_us_east_1 = {
		# name = "bay-cdp-cust-mdm-outbound-job-us-east-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 0 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "cust-mdm-outbound-job"
    #       }
    #      }
    #   }
      glue_trigger_conditional_bay_cdp_mdm_sap_customer_enrich_job_us_east_1 = {
		    name = "bay-cdp-mdm-sap-customer-enrich-job-us-east-1_trigger"
        type = "SCHEDULED"
        schedule = "cron(0 3 ? * * *)"
		    glue_trigger_actions = {
           test = {
              job_name = "mdm-external-source-step-funct-trigger"
              arguments = {
              "--JOB_REGION" = "us-east-1",
              "--TARGET_COUNTRY" = "US",
              "--TARGET_ENRICHED_SOURCE_NAME" = "DATAONE_US",
              "--TARGET_JOB" = "bay-cdp-mdm-sap-customer-enrich-job-us-east-1",
              "--TARGET_LAYER"="Model",
              "--TARGET_SOURCE_NAME"="SAP_CUSTOMER_MDM"
              }
            }
        }
      }
    #   glue_trigger_conditional_bay_cdp_mdm_veeva_network_customer_enrich_job_us_east_1 = {
		# name = "bay-cdp-mdm-veeva-network-customer-enrich-job-us-east-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 5 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "mdm-veeva-network-customer-enrich-job"
    #           arguments = {
    #           "--JOB_COUNTRY" = "US"
    #           }
    #       }
    #      }
    #   }
  }
}

glue_triggers_ap_southeast_1 = {
  landing = {

    glue_trigger_veevatranslation_PC1 = {
       name = "glue_trigger_veevatranslation_PC1"
         }

       }
     }

     # bay_cdp_collibra_glue_catalog_ap_southeast_1_trg
     bay_cdp_collibra_glue_catalog_ap_southeast_1_trg = {
       name = "bay_cdp_collibra_glue_catalog_ap_southeast_1_trg"
       type = "SCHEDULED"
       schedule = "cron(0 13 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "collibra-glue-catalog"
           arguments = {
           "--SOURCE_NAME" = "",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_COUNTRY" = "",
           "--JOB_REGION" = "AP"
         }
         }

       }
     }

      #glue_trigger_japan_connectivity
     /*glue_trigger_japan_connectivity = {
       name = "glue_trigger_japan_connectivity"
       type = "SCHEDULED"
       schedule = "cron(0 18 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "localdata-oracle-connectivity"
           arguments = {
           "--SOURCE_NAME" = "ANALYTICS_JP",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_COUNTRY" = "JP",
           "--JOB_REGION" = "AP"
         }
         }

       }
     }*/
    #  glue_trigger_glue_trigger_bay_cdp_mdm_veeva_enrich_job_ap_southeast_1 = {
		# name = "bay-cdp-mdm-veeva-enrich-job-ap-southeast-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 3/6 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "mdm-veeva-enrich-job"
    #           arguments = {
    #           "--JOB_COUNTRY" = "PC3"
    #           }
    #       }
    #      }
    #   }
      glue_trigger_glue_trigger_bay_cdp_mdm_sap_enrich_job_ap_southeast_1 = {
		name = "bay-cdp-mdm-sap-enrich-job-ap-southeast-1_trigger"
        type = "SCHEDULED"
        schedule = "cron(0 3 ? * * *)"
		glue_trigger_actions = {
          test = {
              job_name = "mdm-external-source-step-funct-trigger"
              arguments = {
              "--JOB_REGION" = "ap-southeast-1",
              "--TARGET_COUNTRY" = "AP",
              "--TARGET_ENRICHED_SOURCE_NAME" = "DATAONE_AP",
              "--TARGET_JOB" = "bay-cdp-mdm-sap-enrich-job-ap-southeast-1",
              "--TARGET_LAYER"="Model",
              "--TARGET_SOURCE_NAME"="SAP_MDM"
              }
          }
         }
      }

      glue_trigger_dq_enriched_model_recon_ap = {
       name = "glue_trigger_dq_enriched_model_recon_ap"
       type = "SCHEDULED"
       schedule = "cron(0 21 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "dq-enriched-model-recon-job"
           arguments = {
           "--SOURCE_NAME" = "DQ_RECON",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_NAME" = "bay-cdp-dq-enriched-model-recon-job-ap-southeast-1",
           "--JOB_REGION" = "ap-southeast-1"
         }
         }

       }
     }

     glue_trigger_dq_landing_enriched_recon_ap = {
       name = "glue_trigger_dq_landing_enriched_recon_ap"
       type = "SCHEDULED"
       schedule = "cron(0 21 ? * * *)"
       glue_trigger_actions = {
         test = {
           job_name = "dq-landing-enriched-recon-job"
           arguments = {
           "--SOURCE_NAME" = "DQ_RECON",
           "--JOB_RUN" = "1",
           "--LOAD_ID" ="1",
           "--JOB_NAME" = "bay-cdp-dq-landing-enriched-recon-job-ap-southeast-1",
           "--JOB_REGION" = "ap-southeast-1"
         }
         }

       }
     }

    #   glue_trigger_conditional_bay_cdp_mdm_veeva_customer_enrich_job_ap_southeast_1_PC3 = {
		# name = "bay-cdp-mdm-veeva-customer-enrich-job-ap-southeast-1_trigger"
    #     type = "SCHEDULED"
    #     schedule = "cron(0 3/6 ? * * *)"
		# glue_trigger_actions = {
    #        test = {
    #           job_name = "mdm-veeva-customer-enrich-job"
    #           arguments = {
    #           "--JOB_COUNTRY" = "PC3"
    #           }
    #       }
    #      }
    #   }

  }
}

